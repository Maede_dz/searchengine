package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;

public class SearchEngine {

    private Scanner scan;
    private File[] files;
    private File dir;

    SearchEngine(){
        scan = new Scanner(System.in);
        dir = new File(".");
        files = dir.listFiles((dir, filename) -> filename.endsWith(".txt"));
    }

    void getWord(){
        while (true){
        try {
            String line = scan.nextLine();
            findOP(line);
        } catch (Exception ex){
            System.out.println("enter valid input");
        }
        }
    }

    private ArrayList<String> searchWord(String word) throws FileNotFoundException {
        ArrayList<String> contains = new ArrayList<>();
        StringBuilder wordBuilder = new StringBuilder(word);
        wordBuilder.append(" ");
        for (File f:files){
            Scanner fscanner = new Scanner(f);
            while (fscanner.hasNextLine()){
                String line = fscanner.nextLine();
                line = line + " ";
                if (line.toLowerCase().contains(wordBuilder.toString().toLowerCase())){
                    contains.add(f.getName());
                }
            }
        }
        return contains;
    }

    private void findOP(String line) throws FileNotFoundException {
        if (line.contains("AND") && line.contains("OR")){
            System.out.println(findFilesWithBothOp(line));
        } else if(line.contains("AND")){
            String[] words = line.split(" AND ");
            System.out.println(findFilesWithANDOp(new ArrayList<>(Arrays.asList(words))));
        } else if(line.contains(" OR ")){
            String[] words = line.split("OR");
            System.out.println(findFilesWithOROp(new ArrayList<>(Arrays.asList(words))));
        } else{
            System.out.println(searchWord(line));
        }
    }

    private HashSet<String> findFilesWithANDOp(ArrayList<String> words) throws FileNotFoundException {
        HashSet<String> set = new HashSet<>(searchWord(words.get(0)));
        for (String w:words){
            set.retainAll(searchWord(w));
        }
        return set;
    }

    private HashSet<String> findFilesWithOROp(ArrayList<String> words) throws FileNotFoundException {
        HashSet<String> orFiles = new HashSet<>(searchWord(words.get(0)));
        for (String w:words){
            orFiles.addAll(searchWord(w));
        }
        return orFiles;
    }
    private HashSet<String> findFilesWithBothOp(String line) throws FileNotFoundException {
        ArrayList<String> words = new ArrayList<>(Arrays.asList(line.split(" AND ")));
        ArrayList<String> orWords = new ArrayList<>(Arrays.asList(words.get(words.size() - 1).split(" OR ")));
        words.remove(words.size() - 1);
        words.add(orWords.get(0));
        HashSet<String> set = new HashSet<>(findFilesWithANDOp(words));
        orWords.remove(0);
        set.addAll(findFilesWithOROp(orWords));
        return set;
    }
}
